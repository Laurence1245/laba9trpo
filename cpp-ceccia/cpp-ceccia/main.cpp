#include <iostream>

using namespace std;
//merge conflict resolved
#include "session.h"
#include "student.h"
#include "constants.h"

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �9. GIT\n";
    cout << "������� �8. Session ends\n";
    cout << "�����: Rusupetov Denis\n";
    cout << "Group: 16\n\n";
    session_result* results[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", results, size);
        for (int i = 0; i < size; i++)
        {
            cout << "Last Name: " << results[i]->student.last_name << '\n';
            cout << "First name: " << results[i]->student.first_name << '\n';
            cout << "Middle name: " << results[i]->student.middle_name << '\n';
            cout << "Date: " << results[i]->exam.day << ' ';
            cout << results[i]->exam.month << ' ';
            cout << results[i]->exam.year << '\n';
            cout << "Mark: " << results[i]->mark << '\n';
            cout << "Discipline: " << results[i]->discipline << '\n';
            cout << '\n';
            
        }
        for (int i = 0; i < size; i++)
        {
            delete results[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}