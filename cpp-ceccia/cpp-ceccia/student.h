#ifndef FILE_READER_H
#define FILE_READER_H

#include "session.h"

void read(const char* file_name, session_result* array[], int& size);

#endif